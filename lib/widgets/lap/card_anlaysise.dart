import 'package:flutter/material.dart';

class Card_anlaysise extends StatefulWidget {
  const Card_anlaysise(
      {Key? key,
      this.press,
      this.Generaldascription,
      this.DrName,
      this.LapName,
      this.all_Analysis,
      this.date,
      this.requestImageID})
      : super(key: key);
  final bool? all_Analysis;
  final VoidCallback? press;

  final String? Generaldascription, DrName, LapName, date, requestImageID;

  @override
  State<Card_anlaysise> createState() => _Card_anlaysiseState();
}

class _Card_anlaysiseState extends State<Card_anlaysise> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, right: 10, left: 10),
      child: Column(
        children: [
          InkWell(
            onTap: widget.press,
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 1,
                    vertical: 1,
                  ),
                  height: MediaQuery.of(context).size.height * 0.12,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(22),
                    color: Colors.white,
                    // ignore: prefer_const_literals_to_create_immutables
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          Text(
                            'الدكتور :',
                            style: TextStyle(
                                color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '${widget.DrName} ',
                            style: TextStyle(
                                color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Checkbox(value: false, onChanged: (all_Analysis) {}),
                            ],
                          )
                        ],
                      ),
                      Container(
                        height: 50,
                        width: 300,
                        decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${widget.Generaldascription}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                              ),
                            ),
                            Text(
                              '${widget.date} ',
                              style: TextStyle(
                                  color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Divider(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
