import 'package:ehr/model/PatientAnalyze.dart';
import 'package:provider/provider.dart';
import 'package:ehr/model/general_DAta/general_data.dart';
import 'package:ehr/widgets/lap/card_anlaysise.dart';
import 'package:flutter/material.dart';
import '../../screen/detail_analysis.dart';

class cards_analysises extends StatefulWidget {
  List<PatientAnalyze>? patAnalyzeList;

  cards_analysises({this.patAnalyzeList});

  @override
  State<cards_analysises> createState() => _cards_analysisesState();
}

class _cards_analysisesState extends State<cards_analysises> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: widget.patAnalyzeList!.length,
        itemBuilder: (context, index) {
          return Card_anlaysise(
            DrName: widget.patAnalyzeList![index].DoctorName,
            Generaldascription: widget.patAnalyzeList![index].Details,
            date: widget.patAnalyzeList![index].Date_encoded,
            requestImageID: "",
            press: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => anlayss_detail()));
            },
          );
        },
      ),
    );
  }
}
