import 'package:flutter/material.dart';

import 'analysistypes.dart';

class cardaddanalysis extends StatefulWidget {
   cardaddanalysis({Key? key , this.isDon}) : super(key: key);
   bool? isDon= true;
  @override
  State<cardaddanalysis> createState() => _cardaddanalysisState();
}

class _cardaddanalysisState extends State<cardaddanalysis> {
  

  
  var max = 10;
  var min = 0;
  List _VALUELIST = ['dr', 'ph', 'lp', 'hosp', 'ad', 'select one'];
  String ?selecteditem;
  @override
  Widget build(BuildContext context) {
    return  
                                  DropdownButton(
                                        hint: Text("choose from list"),
                                        dropdownColor: Color(0xfff2f9fe),
                                        elevation: 5,
                                        icon: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.grey[250],
                                        ),
                                        iconSize: 25,
                                        isExpanded: true,
                                        underline: SizedBox(
                                          height: 10,
                                        ),
                                        style: TextStyle(fontSize: 18, color: Colors.black),
                                        autofocus: true,
                                        value: selecteditem,
                                        onChanged: (val) {
                                          setState(() {
                                            selecteditem = val.toString();
                                          });
                                        },
                                        items: _VALUELIST.map((List) {
                                          return DropdownMenuItem(value: List, child: Text(List));
                                        }).toList());
  }
}
