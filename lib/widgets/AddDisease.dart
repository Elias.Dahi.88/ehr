import 'package:flutter/material.dart';

class AddDisease extends StatelessWidget {
   AddDisease({ Key ?key }) : super(key: key);

   TextEditingController _userNameTD = TextEditingController();
  TextEditingController _postTitleTD = TextEditingController();
  TextEditingController _postTextTD = TextEditingController();

  _clearTextInput() {
    _userNameTD.text = '';
    _postTitleTD.text = '';
    _postTextTD.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                   
                    SizedBox(height: 20),
                    TextField(
                      controller: _postTitleTD,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      decoration: InputDecoration(
                        hintText: 'الاعراض',
                      ),
                    ),
                    SizedBox(height: 20),
                    TextField(
                      controller: _postTextTD,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      decoration: InputDecoration(
                        hintText: 'التشخيص',
                      ),
                    ),
                    SizedBox(height: 20),
                    ElevatedButton(
                      onPressed: () {
                       // _addPost();
                      },
                      child: Text(
                        'نشر الأن',
                        style: TextStyle(
                          fontSize: 25,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
