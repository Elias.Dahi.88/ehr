import 'package:flutter/material.dart';


class PH_Card extends StatefulWidget {
  const PH_Card({ Key ?key ,
   this.press,
    this.Generaldascription,
    this.DrName,
    this.PHName }) : super(key: key);
   final VoidCallback ? press;

   final String ? Generaldascription,DrName,PHName;


  @override
  State<PH_Card> createState() => _PH_CardState();
}

class _PH_CardState extends State<PH_Card> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, right:10, left: 10),
      child: Column(
        children: [
          InkWell(
            onTap:widget.press ,
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 1,
                    vertical: 1,
                  ),
                  height: MediaQuery.of(context).size.height * 0.12,
          
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(22),
                      color: Colors.white,
                      // ignore: prefer_const_literals_to_create_immutables
                      ),
                ),
                SizedBox(
                   height: MediaQuery.of(context).size.height * 0.15,
                  width: MediaQuery.of(context)
                          .size
                          .width -
                      100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          Text(
                            'الدكتور :',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight:
                                    FontWeight.bold),
                          ),
                          Text(
                            '${widget.DrName} ',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight:
                                    FontWeight.bold),
                          ),
                        ],
                      ),
                                         Container(
                        height: 50,
                        width: 300,
                        decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey)),
                        child: Text(
                          '${widget.Generaldascription}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      
                      Divider(),
                      
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}