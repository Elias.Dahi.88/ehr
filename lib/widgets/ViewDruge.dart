import 'package:ehr/screen/DetailDrug.dart';
import 'package:flutter/material.dart';

class ViewDruge extends StatefulWidget {
  String? medicineNAme;
  String? dosage;
  int? period;

  ViewDruge({this.medicineNAme, this.dosage, this.period});

  @override
  State<ViewDruge> createState() => _ViewDrugeState();
}

class _ViewDrugeState extends State<ViewDruge> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDruge()));
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 8, 8, 0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0, top: 8.0),
              child: FittedBox(
                child: Row(
                  children: [
                    Center(
                      child: Text(widget.medicineNAme!,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black)),
                    ),
                    SizedBox(
                      width: 25,
                    ),
                    Container(
                      height: 30,
                      width: 100,
                      child: Text(widget.dosage!,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Container(
                      height: 30,
                      width: 100,
                      child: Text(widget.period!.toString() + 'المدة: ',
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
