import 'package:flutter/material.dart';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

class ViewImage extends StatelessWidget {
   ViewImage({ Key ? key, this.name, this.date, this.lapname}) : super(key: key);
   final String? name,date,lapname;
   
   /*Future pickimage() async{
     final image = await ImagePicker().pickImage(source:)
   }*/
  @override
  Widget build(BuildContext context) {
    return Container(
      height:150.0,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
       
          Container(
            height: 40,
            child: Row(
              children: [
                Text('$name'),
            

               
                ],
            ),
          ),
          
           
            Container(
              height: 35,
              child: Row(
                children: [
                  Text('أٍسم الدكتور'),
                  
                  Text('$lapname'),

                  Padding(
                    padding: const EdgeInsets.only(right:25.0),
                    child: Text('$date'),
                  ),

                  Container(
                    padding: const EdgeInsets.only
                    (right: 20.0),
                    child: Checkbox(value: false, onChanged: (bool ? isDon){}),
                   ) 
                ],
              ),
            ),
            SizedBox(height:12),
            Container(
            height: 40,
            child: Row(
              children: [
              Container(
                            height: 40.0,
                            width: 150.0,
                            color: Colors.blue,
                            child: TextButton.icon(
                                onPressed: () {
                                },
                                icon: Icon(
                                  Icons.analytics_outlined,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                label: Text(' عرض ',
                                    style:
                                        TextStyle(color: Colors.white)))),
                                        SizedBox(width: 15,),
            Container(
                            height: 40.0,
                            width: 150.0,
                            color: Colors.blue,
                            child: TextButton.icon(
                                onPressed: () {

                                },
                                icon: Icon(
                                  Icons.analytics_outlined,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                label: Text(' تحميل ',
                                    style:
                                        TextStyle(color: Colors.white)))),

               
                ],
            ),
          ),
          
          
        ],
      ),

    );
  }
}