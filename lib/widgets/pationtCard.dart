import 'package:ehr/screen/AddCardsAnalysis.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:flutter/material.dart';

class PationtCard extends StatelessWidget {
  final String? PageTitle, Pationtname, Image, Number, AdName;
  final VoidCallback? onpressed;

  const PationtCard({
    Key? key,
    this.Pationtname,
    this.PageTitle,
    this.Image,
    this.Number,
    this.AdName,
    this.onpressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 25.0, left: 25),
      child: Expanded(
        child: Container(
          height: MediaQuery.of(context).size.height * 0.6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              ),
              boxShadow: [
                BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                BoxShadow(offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
              ]),
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.blue,
                  ),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('$Image'),
                    radius: 40.0,
                  ),
                ),
                Text(
                  '$PageTitle',
                  style: TextStyle(color: Colors.blue, fontSize: 20, fontWeight: FontWeight.bold),
                ),
                pationtcard_detail(
                  AdName: Pationtname,
                  static: 'name',
                ),
                pationtcard_detail(
                  AdName: Number,
                  static: 'number:',
                ),
                pationtcard_detail(
                  AdName: AdName,
                  static: 'التأمين',
                ),
                Center(
                  child: Container(
                    decoration: BoxDecoration(color: Colors.transparent, shape: BoxShape.rectangle,

                        // ignore: prefer_const_literals_to_create_immutables
                        boxShadow: [
                          BoxShadow(offset: Offset(0, 5), blurRadius: 15, color: Colors.black38),
                        ]),
                    child: (SharedClass.isDoctorAccount == true)
                        ? FloatingActionButton(
                            onPressed: onpressed,
                            child: Icon(Icons.add, color: Colors.white, size: 25),
                          )
                        : Container(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class pationtcard_detail extends StatelessWidget {
  const pationtcard_detail({
    Key? key,
    @required this.AdName,
    this.static,
  }) : super(key: key);

  final String? static;
  final String? AdName;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25,
      width: 200,
      child: Expanded(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(' $static :  ', style: TextStyle(color: Colors.black)),
            Text('$AdName', style: TextStyle(color: Colors.black)),
          ],
        ),
      ),
      margin: EdgeInsets.only(bottom: 20),
    );
  }
}
