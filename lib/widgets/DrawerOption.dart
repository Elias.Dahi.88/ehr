import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {
  final IconData? icon;
  final String ? text;
  DrawerTile({
   @ required this.icon,
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        icon,
        size: 40,
      ),
      title: Text(
        '$text',
        style: TextStyle(fontSize: 20),
      ),
    );
  }
}
