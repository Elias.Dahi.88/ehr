
import 'package:flutter/material.dart';

class disease_Card extends StatefulWidget {
  const disease_Card({ Key ?key,
    this.press,
    this.Generaldescription,
    this.DrName,
     }) : super(key: key);
   final VoidCallback? press;

   final String? Generaldescription,DrName;

  @override
  State<disease_Card> createState() => _disease_Card();
}

class _disease_Card extends State<disease_Card> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8, right:10, left: 10),
      child: Column(
        children: [
          InkWell(
            onTap:widget.press ,
            child: Stack(
              alignment: Alignment.topRight,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 1,
                    vertical: 1,
                  ),
                  height: MediaQuery.of(context).size.height * 0.12,
          
                  decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(22),
                      color: Colors.white,
                      // ignore: prefer_const_literals_to_create_immutables
                      ),
                ),
                SizedBox(
                   height: MediaQuery.of(context).size.height * 0.15,
                  width: MediaQuery.of(context)
                          .size
                          .width -
                      100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          Text(
                            'الدكتور :',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight:
                                    FontWeight.bold),
                          ),
                          Text(
                            '${widget.DrName} ',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontWeight:
                                    FontWeight.bold),
                          ),
                        ],
                      ),
                      Container(
                        height: 50,
                        width: 300,
                        decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey)),
                        child: Text(
                          '${widget.Generaldescription}',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      
                      Divider(),
                      
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}