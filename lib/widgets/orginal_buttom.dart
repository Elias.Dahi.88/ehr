

import 'package:flutter/material.dart';
class OrginalButtom extends StatelessWidget {
  
  final String ? text;
  final VoidCallback? onpressed;
  const OrginalButtom({ Key? key , @required this.text,@required this.onpressed }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 60),
      child: SizedBox(
        height: 60,
        width: double.infinity,
        child: ElevatedButton(

          onPressed: onpressed,
           child: Text('$text'),
          ),
      ),
    );
  }
}