import 'package:ehr/screen/DetailDrug.dart';
import 'package:flutter/material.dart';

class AlternativeMedicine extends StatelessWidget {
  String? medicineNAme;
  String? dosage;
  int? period;

  AlternativeMedicine({this.medicineNAme, this.dosage, this.period});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailDruge()));
      },
      child: Row(
        children: [
          Container(
            height: 60,
            width: 100,
            child: Center(
              child: Text(medicineNAme!,
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black)),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Row(
            children: [
              Container(
                height: 30,
                width: 80,
                child: Text(dosage!,
                    textAlign: TextAlign.right,
                    style:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
              ),
              SizedBox(
                width: 20,
              ),
              Container(
                height: 30,
                width: 80,
                child: Text(period.toString() + 'المدة: ',
                    textAlign: TextAlign.right,
                    style:
                        TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
