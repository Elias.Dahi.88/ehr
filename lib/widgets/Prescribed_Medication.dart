import 'package:flutter/material.dart';


class Prescribed_Medication extends StatefulWidget {
  const Prescribed_Medication({ Key ?key }) : super(key: key);

  @override
  State<Prescribed_Medication> createState() => _Prescribed_MedicationState();
}

class _Prescribed_MedicationState extends State<Prescribed_Medication> {
  
  List _VALUELIST = ['dr', 'ph', 'lp', 'hosp', 'ad', 'select one'];
  String? selecteditem;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8,8,8,0),
      child: Container(
        height: 150,
        width: MediaQuery.of(context).size.width*0.85,
        child: Column(
          children: [
            Row(
              children: [
                  
                                Container(
                                  height: 55,
                                  width: MediaQuery.of(context).size.width*0.45,
                                  child: DropdownButton(
                                        hint: Text("choose from list"),
                                        dropdownColor: Color(0xfff2f9fe),
                                        elevation: 5,
                                        icon: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.grey[250],
                                        ),
                                        iconSize: 25,
                                        isExpanded: true,
                                        underline: SizedBox(
                                          height: 10,
                                        ),
                                        style: TextStyle(fontSize: 18, color: Colors.black),
                                        autofocus: true,
                                        value: selecteditem,
                                        onChanged: (val) {
                                          setState(() {
                                            selecteditem = val.toString();
                                          });
                                        },
                                        items: _VALUELIST.map((List) {
                                          return DropdownMenuItem(value: List, child: Text(List));
                                        }).toList()),
                                ),
                                SizedBox(width: 25,),
               Column(
                                      children: [
                                        Container(
                                          height: 30,
                                          width: 80,
                                          child: TextField(decoration: InputDecoration(
                  labelText:'الجرعة الدوائية ',
                ),)),

                       SizedBox(height:10,),
                                      Container(
                                      height: 30,
                                      width: 80,
                                      child: TextField(decoration: InputDecoration(
                  labelText:'المدة الزمنية ',
                ),)),

                                      ],
                                    ),
                                    
              ],
            ),
            SizedBox(height: 10,),
             Row(
              children: [
                  
                                Container(
                                  height: 55,
                                  width: MediaQuery.of(context).size.width*0.45,
                                  child: DropdownButton(
                                        hint: Text("الدواء البديل"),
                                        dropdownColor: Color(0xfff2f9fe),
                                        elevation: 5,
                                        icon: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.grey[250],
                                        ),
                                        iconSize: 25,
                                        isExpanded: true,
                                        underline: SizedBox(
                                          height: 10,
                                        ),
                                        style: TextStyle(fontSize: 18, color: Colors.black),
                                        autofocus: true,
                                        value: selecteditem,
                                        onChanged: (val) {
                                          setState(() {
                                            selecteditem = val.toString();
                                          });
                                        },
                                        items: _VALUELIST.map((List) {
                                          return DropdownMenuItem(value: List, child: Text(List));
                                        }).toList()),
                                ),
                                
                                SizedBox(width: 25,),
                                    Column(
                                      children: [
                                        Container(
                                          height: 30,
                                          width: 80,
                                          child: TextField(decoration: InputDecoration(
                  labelText:'الجرعة الدوائية ',
                ),)),

                       SizedBox(height:10,),
                                      Container(
                                      height: 30,
                                      width: 80,
                                      child: TextField(decoration: InputDecoration(
                  labelText:'المدة الزمنية ',
                ),)),

                                      ],
                                    ),
                                

                                    
              ],
            )

        ],),

        
      ),
    );
  }
}