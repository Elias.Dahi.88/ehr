import 'package:flutter/material.dart';

import 'analysistypes.dart';

class order_analysis extends StatefulWidget {
  const order_analysis({ Key? key }) : super(key: key);

  @override
  State<order_analysis> createState() => _order_analysisState();
}

class _order_analysisState extends State<order_analysis> {

  
  var  max=10;
  var  min=0;
    List _VALUELIST = [
    'dr',
    'ph',
    'lp',
    'hosp',
    'ad',
    'select one'];
  String ? selecteditem ;
  @override
  Widget build(BuildContext context) {
    return Column(children: [
        Container(padding: EdgeInsets.only(left: 20,right: 20),
                                          decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black),
                                           
                                            shape: BoxShape.rectangle,
                                          ),
                         child: DropdownButton(
                                                hint: Text("choose from list"),
                                                dropdownColor: Color(0xfff2f9fe),
                                                elevation: 5,
                                                icon: Icon(Icons.arrow_drop_down_circle_outlined,
                                                color: Colors.grey[250],),
                                                iconSize: 25,
                                                isExpanded: true,
                                                underline: SizedBox(
                                                  height: 10,
                                                  ),
                                                
                                                style: TextStyle(fontSize: 18, color: Colors.black),
                                                autofocus: true,
                                                value: selecteditem,
                                                 onChanged: (val){
                                                  setState(() {
                                                    selecteditem=val.toString()  ;
                                                  });
                                                },
                                                items: _VALUELIST.map((List) {
                                                  return DropdownMenuItem(
                                                    value: List,
                                                    child: Text(List));
                                                }).toList(),
                                               
                                                

                                              ),
                                              
                       ),
                       
                                      analysisTypes(max: max, min: min),
                                      ],
                                      );
  }
}