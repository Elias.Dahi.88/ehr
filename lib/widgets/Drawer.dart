import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../screen/home_login.dart';
import 'DrawerOption.dart';

class MainDrawer extends StatefulWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  List _VALUELIST = [
    'التأمينات',
    'أطباء',
    'صيادلة',
    'مخابر',
  ];
  String? selecteditem;
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.grey[200],
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 250,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      scale: 0.75,
                      opacity: .8,
                      image: AssetImage('assets/images/2.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                    bottom: 0,
                    child: Container(
                      width: 350,
                      height: 60,
                      color: Colors.black38,
                      child: Center(
                          child: Text(
                        'وزارة الصحة \n صحتكم في عهدتنا',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      )),
                    ))
              ],
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return Container(); // AccountDialog();
                    });
              },
              child: DrawerTile(
                icon: Icons.person,
                text: 'حسابي',
              ),
            ),
            Divider(
              thickness: 1,
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
              ),
              child: DropdownButton(
                hint: Text("التأمينات"),
                dropdownColor: Colors.grey[290],
                elevation: 5,
                isExpanded: true,
                underline: SizedBox(
                  height: 10,
                ),
                style: TextStyle(fontSize: 18, color: Colors.black),
                autofocus: true,
                value: selecteditem,
                onChanged: (val) {
                  setState(() {
                    selecteditem = val.toString();
                  });
                },
                items: _VALUELIST.map((List) {
                  return DropdownMenuItem(value: List, child: Text(List));
                }).toList(),
              ),
            ),
            Divider(
              thickness: 1,
            ),
            InkWell(
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context, MaterialPageRoute(builder: (context) => homepage()), (route) => false);
              },
              child: DrawerTile(
                icon: Icons.logout_outlined,
                text: 'تسجيل الخروج',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
