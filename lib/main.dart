// @dart=2.9
import 'package:ehr/model/general_DAta/general_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'package:ehr/screen/home_login.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
    runApp(MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        theme: ThemeData(
          primarySwatch: Colors.blue,
          inputDecorationTheme: InputDecorationTheme(
            filled: true,
            fillColor: const Color(0xfff2f9fe),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: BorderRadius.circular(25)),
            disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                borderRadius: BorderRadius.circular(25)),
            focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.lightBlue),
              //borderRadius: BorderRadius.circular(25)
            ),
          ),
          fontFamily: 'Taj',
        ),
        supportedLocales: [Locale('ar')],
        locale: Locale('ar'),
        debugShowCheckedModeBanner: false,
        title: 'E.H.R Application',
        //  routes: {},
        home: AnimatedSplashScreen(
            splash: 'assets/images/1.png',
            splashTransition: SplashTransition.fadeTransition,
            centered: true,
            backgroundColor: Colors.white, // HexColor('008080'),
            animationDuration: Duration(
              seconds: 1,
            ),
            pageTransitionType: PageTransitionType.fade,
            splashIconSize: 200,
            nextScreen: homepage()));
  }
}
