class PatientAnalyze {
  int? AnalyzeID;
  int? Patient_ID;
  int? Doctor_ID;
  String? DoctorName;
  String? Details;
  String? Date_encoded;

  PatientAnalyze(
      {this.AnalyzeID,
      this.Patient_ID,
      this.Doctor_ID,
      this.DoctorName,
      this.Details,
      this.Date_encoded});

  factory PatientAnalyze.fromJSON(Map<String, dynamic> json) {
    return PatientAnalyze(
        AnalyzeID: json['AnalyzeID'],
        Patient_ID: json['Patient_ID'],
        Doctor_ID: json['Doctor_ID'],
        DoctorName: json['DoctorName'],
        Details: json['Details'],
        Date_encoded: json['Date_encoded']);
  }
}
