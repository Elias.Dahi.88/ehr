class News {
  int? NewsID;
  int? ActorID;
  String? ActorName;
  String? Title;
  String? Details;

  News({this.NewsID, this.ActorID, this.ActorName, this.Title, this.Details});

  factory News.fromJSON(Map<String, dynamic> json) {
    return News(
        NewsID: json['NewsID'],
        ActorID: json['ActorID'],
        ActorName: json['ActorName'],
        Title: json['Title'],
        Details: json['Details']);
  }
}
