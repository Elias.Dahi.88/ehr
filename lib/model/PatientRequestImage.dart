class PatientRequestImage {
  int? RequestImagesID;
  int? Patient_ID;
  int? Doctor_ID;
  String? DoctorName;
  String? PatientName;
  String? Details;
  String? Date_encoded;

  PatientRequestImage(
      {this.RequestImagesID,
      this.Patient_ID,
      this.Doctor_ID,
      this.DoctorName,
      this.PatientName,
      this.Details,
      this.Date_encoded});

  factory PatientRequestImage.fromJSON(Map<String, dynamic> json) {
    return PatientRequestImage(
        RequestImagesID: json['RequestImagesID'],
        Patient_ID: json['Patient_ID'],
        Doctor_ID: json['Doctor_ID'],
        DoctorName: json['DoctorName'],
        PatientName: json['PatientName'],
        Details: json['Details'],
        Date_encoded: json['Date_encoded']);
  }
}
