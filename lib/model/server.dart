import 'package:http/http.dart' as http;
import 'dart:convert';

class server {
  // TODO: to be changed with your laptop IP
  static const Root = 'http://172.20.10.8:8000';
  static const imageRoot = 'http://172.20.10.8:8000/media';
  // static const Root = 'http://192.168.1.106:8000';

  /*
  ****************** Patient API URLs
  * */
  static final String SetPatientApiURL = "${Root}/SetPatientApi";
  static final String GetPatientURL = "${Root}/GetPatient";
  static final String LoginPatientURL = "${Root}/LoginPatient";
  static final String ViewDiagnosisDiseasesURL = "${Root}/ViewDiagnosisDiseases";
  static final String ViewPatientDiagnosisURL = "${Root}/ViewPatientDiagnosis";
  static final String ViewPatientPrescriptionsURL = "${Root}/ViewPatientPrescriptions";
  static final String ViewPrescriptionMedicineURL = "${Root}/ViewPrescriptionMedicines";
  static final String ViewPatientAnalyzeURL = "${Root}/ViewPatientAnalyze";
  static final String ViewPatientRequestImageURL = "${Root}/ViewPatientRequestImages";
  static final String ViewNewsURL = "${Root}/ViewNews";
  static final String ViewImageItemURL = "${Root}/ViewImageItem";

  /*
  *
  * *********  Doctor API URLs  ****************
  *
  *
  * */
  static final String LoginDoctorURL = "${Root}/LoginActor";
  static final String AddItemAnalyzeURL = "${Root}/AddItemAnalyze";
  static final String RequestAnalyzeURL = "${Root}/RequestAnalyze";
  static final String AddDiagnosesURL = "${Root}/AddDiagnoses";
  static final String AddItemDiagnosesURL = "${Root}/AddItemDiagnoses";
  static final String AddItemRadialImageURL = "${Root}/AddItemRadialImage";
  static final String AddPrescriptionURL = "${Root}/AddPrescription";
  static final String AddItemPrescriptionURL = "${Root}/AddItemPrescription";
  static final String AddRequestImageURL = "${Root}/AddRequestImage";

  static Future<String> Register(
      {String? Name,
      String? National_Number,
      String? Email,
      String? Password,
      DateTime? birth_Date}) async {
    try {
      print("start register");
      Map<String, dynamic> map = {};
      map['Name'] = Name;
      map['National_Number'] = National_Number;
      map['Email'] = Email;
      map['Password'] = Password;
      map['birth_Date'] = birth_Date.toString();

      print(map);

      Map<String, String> myHeaders = {
        "Accept": "application/json",
        "Referer": "https://ehrsystem.com"
      };

      print(SetPatientApiURL);

      final response = await http.post(
        Uri.parse(SetPatientApiURL),
        body: jsonEncode(map),
        headers: myHeaders,
      );
      print('ADD Post Response: ${response.body}');
      if (response.statusCode == 200) {
        print(jsonDecode(response.body));
        return jsonDecode(response.body);
      } else {
        return 'error';
      }
    } catch (e) {
      return e.toString();
    }
  }

  static Future<int> LoginPatient({String? National_Number, String? Password}) async {
    try {
      print("start Login");
      Map<String, dynamic> bodyRequest = {};
      bodyRequest['National_Number'] = National_Number;
      bodyRequest['Password'] = Password;

      print(bodyRequest);

      Map<String, String> myHeaders = {
        "Accept": "application/json",
        "Referer": "https://ehrsystem.com"
      };

      print(LoginPatientURL);

      final response = await http.post(
        Uri.parse(LoginPatientURL),
        body: jsonEncode(bodyRequest),
        headers: myHeaders,
      );
      print('ADD Post Response: ${response.body}');
      if (response.statusCode == 200) {
        print(jsonDecode(response.body));
        return jsonDecode(response.body);
      } else {
        return -1;
      }
    } catch (e) {
      print(e.toString());
      return -1;
    }
  }

  static Future<List<dynamic>> getPatientRelatedAPIs(
      {Map<String, dynamic>? bodyRequest, String? url}) async {
    try {
      print("Start ${url} API");

      print("bodyRequest\n${bodyRequest}");
      print("Request API URL:\t\t${url}");

      Map<String, String> myHeaders = {
        "Accept": "application/json",
        "Referer": "https://ehrsystem.com"
      };

      final response =
          await http.post(Uri.parse(url!), headers: myHeaders, body: jsonEncode(bodyRequest));

      print('ADD Post Response: ${response.body}');
      if (response.statusCode == 200) {
        print(jsonEncode(response.body));
        List<dynamic> returnedData = jsonDecode(response.body);
        return returnedData;
      } else {
        return [];
      }
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  static Future<List<dynamic>> getAllNews() async {
    try {
      final response = await http.get(Uri.parse(ViewNewsURL));

      if (response.statusCode == 200) {
        print(jsonEncode(response.body));
        List<dynamic> returnedData = jsonDecode(response.body);
        return returnedData;
      } else {
        return [];
      }
    } on Exception catch (e) {
      print(e.toString());
      return [];
    }
  }

  static Future<int> LoginDoctor({String? National_Number, String? Password}) async {
    try {
      print("start Login");
      Map<String, dynamic> bodyRequest = {};
      bodyRequest['National_Number'] = National_Number;
      bodyRequest['Password'] = Password;

      print(bodyRequest);

      Map<String, String> myHeaders = {
        "Accept": "application/json",
        "Referer": "https://ehrsystem.com"
      };

      print(LoginDoctorURL);

      final response = await http.post(
        Uri.parse(LoginDoctorURL),
        body: jsonEncode(bodyRequest),
        headers: myHeaders,
      );
      print('ADD Post Response: ${response.body}');
      if (response.statusCode == 200) {
        print(jsonDecode(response.body));
        return jsonDecode(response.body);
      } else {
        return -1;
      }
    } catch (e) {
      print(e.toString());
      return -1;
    }
  }

  static Future<String> getDoctorRelatedAPIs(
      {Map<String, dynamic>? bodyRequest, String? url}) async {
    try {
      print("Start ${url} API");

      print("bodyRequest\n${bodyRequest}");
      print("Request API URL:\t\t${url}");

      Map<String, String> myHeaders = {
        "Accept": "application/json",
        "Referer": "https://ehrsystem.com"
      };

      final response =
          await http.post(Uri.parse(url!), headers: myHeaders, body: jsonEncode(bodyRequest));

      print('ADD Post Response: ${response.body}');
      if (response.statusCode == 200) {
        print(response.body);
        return response.body;
      } else {
        return response.body;
      }
    } catch (e) {
      print(e.toString());
      return "يوجد خطأ في اتصالك بالانترنيت، يرجى التحقق.";
    }
  }
}
