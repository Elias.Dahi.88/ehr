import 'package:ehr/model/Patient/Rwcorddisease/DetailDisease.dart';
import 'package:ehr/model/Patient/Rwcorddisease/disease.dart';
import 'package:ehr/model/Patient/analysis/ImageX.dart';
import 'package:ehr/model/Patient/analysis/analysis.dart';
import 'package:ehr/model/Patient/analysis/model_card_analysise.dart';
import 'package:ehr/model/Patient/Patient.dart';
import 'package:ehr/model/Patient/Perscription/Drugs.dart';
import 'package:ehr/model/Patient/Perscription/RecordDruges.dart';
import 'package:ehr/model/Patient/Perscription/prescription.dart';
import 'package:ehr/model/user.dart';
import 'package:flutter/material.dart';

class GeneralData extends ChangeNotifier {
  Patient _Pa = Patient();
  Patient get Pa => _Pa;

  set Pa(Patient Pa) {
    _Pa = Pa;
  }

  List<Admain> admain = [
    Admain(id: null, name: 'ahmad', password: '0255535', type: 'ad', valid: null),
    Admain(id: null, name: 'mhamad', password: '0255535', type: 'dr', valid: null),
    Admain(id: null, name: 'yousef', password: '0255535', type: 'ph', valid: null),
    Admain(id: null, name: 'mhmoaud', password: '0255535', type: 'lap', valid: null),
  ];

  List<model_card_analysise> CardAnalysise = [
    model_card_analysise('أحمد', 'يوسف', 'تحليل سكري', 'ط', false, [
      Analysis(name: 'سكري', heighrang: 50, lowrang: 20),
      Analysis(name: 'صفيحات الدم الحمراء', heighrang: 50000, lowrang: 20000),
      Analysis(name: 'بنكرياس', heighrang: 0, lowrang: 1),
    ], []),
    model_card_analysise('أحمد', 'وياد', 'تحليل سكري', 'ط', false, [
      Analysis(name: 'سكري', heighrang: 50, lowrang: 20),
      Analysis(name: 'دقات القلب', heighrang: 90, lowrang: 65),
    ], [
      ImageX('للقلب', '0', '204/2005', 'محمود', false, 'kljghfvbnmh'),
    ])
  ];

  List<Prescription> pre = [
    Prescription(
        'uf]عبد الرحمن',
        DateTime.utc(2015, 5, 8),
        [
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('1', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('2', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('3', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ])
        ],
        false),
    Prescription(
        'khj',
        DateTime.utc(2015, 5, 8),
        [
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ])
        ],
        false),
    Prescription(
        'khj',
        DateTime.utc(2015, 5, 8),
        [
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ]),
          Drugs('name', 'time', 'dosage', false, 'total', [
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation'),
            RecordDruges('id', 'pHDr', DateTime.utc(2015, 5, 8), 'qExpatriation')
          ])
        ],
        false),
  ];
  List<Disease> disease = [
    Disease(
        dateTime: DateTime(2014, 5, 6),
        description: 'kljfhksdjjk',
        doctor: 'mmiodfh',
        detaildisease: [
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name')
        ]),
    Disease(
        dateTime: DateTime(2014, 5, 6),
        description: 'kljfhksdjjk',
        doctor: 'mmiodfh',
        detaildisease: [
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name')
        ]),
    Disease(
        dateTime: DateTime(2014, 5, 6),
        description: 'kljfhksdjjk',
        doctor: 'mmiodfh',
        detaildisease: [
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name'),
          DetailDisease('id', 'name')
        ])
  ];
}
