import 'package:ehr/model/Patient/Rwcorddisease/DetailDisease.dart';

class Disease {
  final int? id;
  final String? doctor, description;
  final DateTime? dateTime;
  List<DetailDisease>? detaildisease = [];

  Disease({this.dateTime, this.description, this.doctor, this.id, this.detaildisease});
}
