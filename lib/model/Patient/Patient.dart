import 'package:ehr/model/Patient/analysis/model_card_analysise.dart';
import 'package:ehr/model/Patient/Perscription/prescription.dart';
import 'package:flutter/cupertino.dart';
import 'Rwcorddisease/disease.dart';

class Patient {
  String? PatientID;
  String? Name;
  String? National_Number;
  String? Email;
  String? Password;
  String? birth_Date;
  String? Age;

  Patient(
      {this.PatientID,
      this.Name,
      this.National_Number,
      this.Email,
      this.Password,
      this.birth_Date,
      this.Age});

  factory Patient.fromJSON(Map<String, dynamic> json) {
    return Patient(
        PatientID: json['PatientID'],
        Name: json['Name'],
        National_Number: json['National_Number'],
        Email: json['Email'],
        Password: json['Password'],
        birth_Date: json['birth_Date'],
        Age: json['Age']);
  }
}
