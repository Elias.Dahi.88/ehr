import 'package:ehr/model/Patient/analysis/ImageX.dart';
import 'package:ehr/model/Patient/analysis/model_card_analysise.dart';
import 'package:flutter/cupertino.dart';

import 'analysis.dart';

class model_card_analysise {
  String drname, patientName, notes, descrption;
  bool isDon;
  List<Analysis> analysis = [];
  List<ImageX> images = [];
  model_card_analysise(@required this.patientName, @required this.drname, this.descrption,
      this.notes, this.isDon, this.analysis, this.images);
  void doneChange() {
    isDon = !isDon;
  }
}
