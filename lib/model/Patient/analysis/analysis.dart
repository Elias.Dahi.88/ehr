import 'package:flutter/cupertino.dart';

class Analysis{
  final String ?name, lapname;
  int ?lowrang,heighrang,result,id;

  bool ? isDone;

  Analysis( {@required this.name, this.lapname,@required this.heighrang,@required this.lowrang,this.result,this.id,this.isDone=false});

  void doneChange(){
    isDone =!isDone!;
  }
}
