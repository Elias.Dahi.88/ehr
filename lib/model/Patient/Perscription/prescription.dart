// ignore_for_file: non_constant_identifier_names

import 'package:ehr/model/Patient/Perscription/Drugs.dart';
import 'package:ehr/model/Patient/Perscription/RecordDruges.dart';

class Prescription {
  final String Drname;
  final DateTime WrDate;
  List<Drugs> drug = [];
  bool isDonPrescription;
  Prescription(this.Drname, this.WrDate, this.drug, this.isDonPrescription);
  void doneChange() {
    isDonPrescription = !isDonPrescription;
  }
}
