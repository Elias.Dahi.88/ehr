import 'package:ehr/model/Patient/Perscription/RecordDruges.dart' show RecordDruges;

class Drugs {
  String name, dosage, time, total;

  bool isGDone;

  List<RecordDruges> recorddauges = [];
  Drugs(this.name, this.time, this.dosage, this.isGDone, this.total, this.recorddauges);
  void doneChange() {
    isGDone = !isGDone;
  }
}
