class ImageItem {
  int? ImageItemID;
  int? RequestImagesID;
  int? RadialImageID;
  String? ImageName;
  int? Analyst_ID;
  String? AnalystName;
  String? Date_encoded;
  String? ImageUrl;

  ImageItem(
      {this.ImageItemID,
      this.RequestImagesID,
      this.RadialImageID,
      this.ImageName,
      this.Analyst_ID,
      this.AnalystName,
      this.Date_encoded,
      this.ImageUrl});

  factory ImageItem.fromJSON(Map<String, dynamic> json) {
    return ImageItem(
        ImageItemID: json['ImageItemID'],
        RequestImagesID: json['RequestImagesID'],
        RadialImageID: json['RadialImageID'],
        ImageName: json['ImageName'],
        Analyst_ID: json['Analyst_ID'],
        AnalystName: json['AnalystName'],
        Date_encoded: json['Date_encoded'],
        ImageUrl: json['ImageUrl']);
  }
}
