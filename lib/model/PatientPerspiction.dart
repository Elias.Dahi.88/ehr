class PatienPerspiction {
  int? PrescriptionID;
  int? Patient_ID;
  int? Doctor_ID;
  String? DoctorName;
  String? Title;
  String? Date_encoded;

  PatienPerspiction(
      {this.PrescriptionID,
      this.Patient_ID,
      this.Doctor_ID,
      this.DoctorName,
      this.Title,
      this.Date_encoded});

  factory PatienPerspiction.fromJSON(Map<String, dynamic> json) {
    return PatienPerspiction(
        PrescriptionID: json['PrescriptionID'],
        Patient_ID: json['Patient_ID'],
        Doctor_ID: json['Doctor_ID'],
        DoctorName: json['DoctorName'],
        Title: json['Title'],
        Date_encoded: json['Date_encoded']);
  }
}
