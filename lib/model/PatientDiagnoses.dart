class PatientDiagnosis {
  int? DiagnosisID;
  int? Patient_ID;
  int? Doctor_ID;
  String? DoctorName;
  String? Details;
  String? Date_encoded;

  PatientDiagnosis(
      {this.DiagnosisID,
      this.Patient_ID,
      this.Doctor_ID,
      this.DoctorName,
      this.Details,
      this.Date_encoded});

  factory PatientDiagnosis.fromJSON(Map<String, dynamic> json) {
    return PatientDiagnosis(
        DiagnosisID: json['DiagnosisID'],
        Patient_ID: json['Patient_ID'],
        Doctor_ID: json['Doctor_ID'],
        DoctorName: json['DoctorName'],
        Details: json['Details'],
        Date_encoded: json['Date_encoded']);
  }
}
