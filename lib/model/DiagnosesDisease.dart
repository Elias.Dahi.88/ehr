class DiagnosesDisease {
  int? DiagnosisDiseaseID;
  int? DiagnosisID;
  int? DiseaseID;
  String? DiseaseName;

  DiagnosesDisease({this.DiagnosisDiseaseID, this.DiagnosisID, this.DiseaseID, this.DiseaseName});

  factory DiagnosesDisease.fromJSON(Map<String, dynamic> json) {
    return DiagnosesDisease(
        DiagnosisDiseaseID: json['DiagnosisDiseaseID'],
        DiagnosisID: json['DiagnosisID'],
        DiseaseID: json['DiseaseID'],
        DiseaseName: json['DiseaseName']);
  }
}
