import 'package:ehr/model/News/post.dart';

import 'package:ehr/model/News/post.dart';

class Posts{
  final List<dynamic>?posts;
  Posts({this.posts});

  factory Posts.fromJson(Map<String,dynamic>json)
  {
    return Posts(
        posts: json['articles'],
    );
  }
}