import 'package:flutter/foundation.dart';

class Post {
 final String ? title,description, imageurl, artical;

Post({
  this.artical,
  this.description,
  this.imageurl,
  this.title
});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      title: json['title'] as String,
      description: json['description'] as String,
      imageurl: json['url'] as String,
      artical: json['urlToImage'] as String,
    );
  }
}
