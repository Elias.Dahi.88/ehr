class PrescriptionMedicine {
  int? PreMedicinesID;
  int? PrescriptionID;
  int? MedicineID;
  String? MedicineName;
  int? AltMedicine;
  String? AltName;
  int? MedicineQuantity;
  String? dosage;
  int? Period;
  int? Pharmacist_ID;
  String? PharmacistName;
  String? Date_encoded;

  PrescriptionMedicine(
      {this.PreMedicinesID,
      this.PrescriptionID,
      this.MedicineID,
      this.MedicineName,
      this.AltMedicine,
      this.AltName,
      this.MedicineQuantity,
      this.dosage,
      this.Period,
      this.Pharmacist_ID,
      this.PharmacistName,
      this.Date_encoded});

  factory PrescriptionMedicine.fromJSON(Map<String, dynamic> json) {
    return PrescriptionMedicine(
        PreMedicinesID: json['PreMedicinesID'],
        PrescriptionID: json['PrescriptionID'],
        MedicineID: json['MedicineID'],
        MedicineName: json['MedicineName'],
        AltMedicine: json['AltMedicine'],
        AltName: json['AltName'],
        MedicineQuantity: json['MedicineQuantity'],
        dosage: json['dosage'],
        Period: json['Period'],
        Pharmacist_ID: json['Pharmacist_ID'],
        PharmacistName: json['PharmacistName'],
        Date_encoded: json['Date_encoded']);
  }
}
