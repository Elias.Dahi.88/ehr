import 'package:ehr/model/PatientPerspiction.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/screen/Addprescription.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:ehr/widgets/PH_Card.dart';
import 'package:ehr/widgets/pationtCard.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:flutter/material.dart';

import 'package:ehr/screen/detail_prescription.dart';

class Hos_prescription extends StatefulWidget {
  @override
  State<Hos_prescription> createState() => _Hos_prescriptionState();
}

class _Hos_prescriptionState extends State<Hos_prescription> {
  var Pationtname = "احمد";
  var PageTitle = 'وصفات طبية';
  var image = 'assets/images/icon_15.png';
  var Number = '03251010548';
  var AddName = 'عرب للتامين الصحي';
  var DrName = 'عبد الرحمن ';
  var PHName = 'روعة محمد';
  var Description = 'وصفة طبية لضغط و سكري';

  var _future;
  Map<String, dynamic> bodyRequest = {};
  List<PatienPerspiction> patientPerspictionList = [];

  @override
  void initState() {
    bodyRequest = {};
    bodyRequest['PatientID'] = int.parse(SharedClass.LoginPatientID);
    _future = server
        .getPatientRelatedAPIs(url: server.ViewPatientPrescriptionsURL, bodyRequest: bodyRequest)
        .then((value) {
      patientPerspictionList = (value.map((e) => PatienPerspiction.fromJSON(e))).toList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: EnhancedFutureBuilder(
        future: _future,
        rememberFutureResult: false,
        whenDone: (whenDone) {
          return SingleChildScrollView(
            child: SizedBox(
              height: 800.0,
              child: Column(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(right: 25, left: 25),
                    child: Column(
                      children: [
                        PationtCard(
                          Pationtname: Pationtname,
                          PageTitle: PageTitle,
                          Image: image,
                          Number: Number,
                          AdName: AddName,
                          onpressed: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) => Addprescription()));
                          },
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Padding(
                            padding: const EdgeInsets.only(right: 25.0, left: 25),
                            child: Expanded(
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.6,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.all(Radius.circular(30)),
                                    // ignore: prefer_const_literals_to_create_immutables
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                      BoxShadow(
                                          offset: Offset(-4, -4),
                                          blurRadius: 15,
                                          color: Colors.white60),
                                    ]),
                                child: ListView.builder(
                                  itemCount: patientPerspictionList.length,
                                  itemBuilder: (context, index) => PH_Card(
                                    DrName: patientPerspictionList[index].DoctorName,
                                    PHName: PHName,
                                    Generaldascription: patientPerspictionList[index].Title,
                                    press: () {
                                      Navigator.of(context).push(MaterialPageRoute(
                                          builder: (context) => detail_prscription(
                                                doctorName:
                                                    patientPerspictionList[index].DoctorName,
                                                perspictionID:
                                                    patientPerspictionList[index].PrescriptionID,
                                              )));
                                    },
                                  ),
                                ),
                              ),
                            )),
                      ],
                    ),
                  ))
                ],
              ),
            ),
          );
        },
        whenNotDone: const Center(
            child: CircularProgressIndicator(
          color: Colors.blue,
        )),
      ),
    );
  }
}
