import 'package:ehr/model/Patient/Perscription/Drugs.dart';
import 'package:flutter/material.dart';

import '../widgets/Prescribed_Medication.dart';

class Addprescription extends StatefulWidget {
  const Addprescription({Key? key}) : super(key: key);

  @override
  State<Addprescription> createState() => _AddprescriptionState();
}

class _AddprescriptionState extends State<Addprescription> {
  List<Drugs> vertual_druge = [];
  List<Drugs> final_druge = [];
  int? max, min;
  String? selecteditem;
  List _VALUELIST = ['dr', 'ph', 'lp', 'hosp', 'ad', 'select one'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 25, 15, 25),
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(30)),
                // ignore: prefer_const_literals_to_create_immutables
                boxShadow: [
                  BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                  BoxShadow(offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                ]),
            child: Center(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(5, 20, 5, 0),
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 1),
                        child: Container(
                          height: 40,
                          width: 250,
                          child: Text('إضافة تحاليل جديدة  ',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 1),
                        child: Container(
                          height: 40,
                          width: 250,
                          child: Text('لسيد/ة : محمد أحمد',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black)),
                        ),
                      ),
                      Divider(
                        height: 10,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Column(children: [
                        Container(
                            height: 40.0,
                            width: 125.0,
                            color: Colors.blue,
                            child: TextButton.icon(
                                onPressed: () {
                                  setState(() {
                                    vertual_druge
                                        .add(Drugs('name', 'time', 'dosage', false, 'total', []));
                                  });
                                },
                                icon: Icon(
                                  Icons.local_pharmacy_outlined,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                label: Text(' دواء جديد ', style: TextStyle(color: Colors.white)))),
                        Divider(
                          height: 15,
                        ),
                        Container(
                          height: 525,
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListView.builder(
                              itemCount: vertual_druge.length,
                              itemBuilder: (context, index) => Container(
                                    height: 170.0,
                                    width: MediaQuery.of(context).size.width,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(30)),
                                      // ignore: prefer_const_literals_to_create_immutables
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(right: 15, left: 15),
                                      child: Column(
                                        children: [
                                          Prescribed_Medication(),
                                        ],
                                      ),
                                    ),
                                  )),
                        ),
                        Divider(
                          height: 15,
                        ),
                        Container(
                          height: 40.0,
                          width: 125.0,
                          color: Colors.blue,
                          child: TextButton.icon(
                              onPressed: () {
                                setState(() {});
                              },
                              icon: Icon(
                                Icons.save,
                                color: Colors.white,
                                size: 30,
                              ),
                              label: Text(' Save ', style: TextStyle(color: Colors.white))),
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ])
                    ]))),
          ),
        ),
      ),
    )));
  }
}
