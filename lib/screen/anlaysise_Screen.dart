import 'package:ehr/model/PatientAnalyze.dart';
import 'package:ehr/model/general_DAta/general_data.dart';
import 'package:ehr/model/Patient/analysis/ImageX.dart';
import 'package:ehr/model/Patient/analysis/analysis.dart';
import 'package:ehr/model/Patient/analysis/model_card_analysise.dart';
import 'package:ehr/model/Patient/Patient.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/screen/AddCardsAnalysis.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:ehr/widgets/lap/card_anlaysise.dart';
import 'package:ehr/widgets/lap/cards_analysises.dart';
import 'package:ehr/widgets/pationtCard.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:provider/provider.dart';

import 'package:flutter/material.dart';

import 'detail_analysis.dart';

class analyssise extends StatefulWidget {
  const analyssise({Key? key}) : super(key: key);

  @override
  State<analyssise> createState() => _analyssiseState();
}

class _analyssiseState extends State<analyssise> {
  var Pationtname = "احمد";
  var PageTitle = 'التحاليل';
  var image = 'assets/images/R.png';
  var Number = '03251010548';
  var AddName = 'عرب للتامين الصحي';
  var DrName = 'عبد الرحمن ';
  var LapName = 'محمد أحمد';
  var Description = 'تحليل لشحوم الثلاثية';

  Map<String, dynamic> bodyRequest = {};
  List<PatientAnalyze> patientAnalyzeList = [];
  var _future;

  @override
  void initState() {
    bodyRequest = {};

    bodyRequest['PatientID'] = int.parse(SharedClass.LoginPatientID);
    _future = server
        .getPatientRelatedAPIs(url: server.ViewPatientAnalyzeURL, bodyRequest: bodyRequest)
        .then((value) {
      patientAnalyzeList = (value.map((e) => PatientAnalyze.fromJSON(e))).toList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: EnhancedFutureBuilder(
          future: _future,
          rememberFutureResult: false,
          whenDone: (whenDone) {
            return SafeArea(
              child: Column(
                children: [
                  Expanded(
                      child: Padding(
                    padding: const EdgeInsets.only(right: 25, left: 25),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          PationtCard(
                            // TODO: to be linked with API
                            Pationtname: "",
                            PageTitle: PageTitle,
                            Image: image,
                            Number: "",
                            AdName: AddName,
                            onpressed: () {
                              Navigator.of(context).push(
                                  MaterialPageRoute(builder: (context) => AddCardsAnalysis()));
                            },
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 25.0, left: 25),
                            child: Expanded(
                              child: Container(
                                height: MediaQuery.of(context).size.height * 0.445,
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(30),
                                      topRight: Radius.circular(30),
                                    ),
                                    // ignore: prefer_const_literals_to_create_immutables
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                      BoxShadow(
                                          offset: Offset(-4, -4),
                                          blurRadius: 15,
                                          color: Colors.white60),
                                    ]),
                                child: Container(
                                    margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
                                    decoration: const BoxDecoration(
                                      color: Colors.white,
                                    ),
                                    child: cards_analysises(
                                      patAnalyzeList: patientAnalyzeList,
                                    )),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
                ],
              ),
            );
          },
          whenNotDone: const Center(
              child: CircularProgressIndicator(
            color: Colors.blue,
          )),
        ));
  }
}
