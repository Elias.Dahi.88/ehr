import 'package:ehr/model/Patient/Patient.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/screen/PationtRegister.dart';
import 'package:flutter/material.dart';

import '../widgets/orginal_buttom.dart';

class Get_Pationt extends StatefulWidget {
  const Get_Pationt({
    Key? key,
  }) : super(key: key);

  @override
  State<Get_Pationt> createState() => _GetPationtState();
}

class _GetPationtState extends State<Get_Pationt> {
  bool is_empty = true;
  void put_empty() => setState(() {
        is_empty = !is_empty;
      });

  TextEditingController patientName = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(50, 50, 50, 50),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    // ignore: prefer_const_literals_to_create_immutables
                    boxShadow: [
                      BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                      BoxShadow(offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                    ]),
                child: Center(
                    child: Column(
                  children: [
                    SizedBox(height: 50),
                    Center(
                        child: Image.asset(
                      'assets/images/5.png',
                      height: 100,
                    )),
                    Divider(height: 40),
                    SizedBox(height: 30),
                    Text(
                      'الرجاء إدخال بيانات المريض : ',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    SizedBox(height: 30),
                    Column(
                      children: [
                        SizedBox(
                            height: 50,
                            width: 250,
                            child: TextFormField(
                              decoration: new InputDecoration(hintText: "أدخل اسم المريض"),
                              controller: patientName,
                            )),
                        SizedBox(height: 10),
                        ElevatedButton(
                            child: Text('Get Patient'),
                            onPressed: () async {
                              Map<String, dynamic> bodyRequest = {};
                              bodyRequest['Patient_Name'] = patientName.text;
                              final result = await server.getPatientRelatedAPIs(
                                  url: server.GetPatientURL, bodyRequest: bodyRequest);

                              if (result[0].Name == "") {
                                print("Patient Not Found");
                              } else {
                                print("Patient found");
                                print(result[0].National_Number);
                              }
                            }),
                        const Divider(height: 100),
                        const SizedBox(height: 40),
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 8.0, bottom: 20.0),
                              child: TextButton.icon(
                                onPressed: () {
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) => PationtRegister()));
                                },
                                icon: Icon(
                                  Icons.add_moderator_outlined,
                                  size: 30,
                                  color: Colors.black,
                                ),
                                label: Text(
                                  'يمكنك إضافة سجل صحي ',
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ))),
          ),
        ),
      ),
    )));
  }
}
