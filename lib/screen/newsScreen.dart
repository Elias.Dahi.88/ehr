import 'package:ehr/model/News.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/screen/CreatePost.dart';
import 'package:ehr/widgets/Drawer.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ehr/widgets/orginal_buttom.dart';

class news extends StatefulWidget {
  @override
  State<news> createState() => _newsState();
}

class _newsState extends State<news> {
  List<News> allNewsList = [];
  var _future;

  @override
  void initState() {
    _future = server.getAllNews().then((value) {
      allNewsList = (value.map((e) => News.fromJSON(e))).toList();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: Text("أخر الاخبار الطبية ")),
          actions: [
            IconButton(
              onPressed: () {
                showModalBottomSheet(
                  backgroundColor: Colors.transparent,
                  barrierColor: Colors.white38,
                  context: context,
                  builder: (context) {
                    return CreatePost();
                  },
                );
              },
              icon: Icon(Icons.add),
            ),
          ],
        ),
        drawer: MainDrawer(),
        body: SingleChildScrollView(
          child: EnhancedFutureBuilder(
            future: _future,
            rememberFutureResult: false,
            whenDone: (whenDone) {
              return GestureDetector(
                  onTap: () {
                    FocusScopeNode currentFocus = FocusScope.of(context);

                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                  },
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Column(children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: GestureDetector(
                            child: Container(
                              height: 75,
                              width: MediaQuery.of(context).size.width * 0.9,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(30)),
                                  // ignore: prefer_const_literals_to_create_immutables
                                  boxShadow: [
                                    BoxShadow(
                                        offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                    BoxShadow(
                                        offset: Offset(-4, -4),
                                        blurRadius: 15,
                                        color: Colors.white60),
                                  ]),
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: FittedBox(
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundImage: AssetImage('assets/images/R (1).png'),
                                        radius: 40.0,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        height: 50,
                                        width: 250,
                                        /* child: TextFormField(
                                        decoration: InputDecoration(
                                      labelText: 'أنصح الاخرين بما لديك',
                                      hintText: 'أكتب ما فيه فائده للاخرين',
                                    )
                                    ),*/
                                        child: Text(
                                          'الوقاية خير من العلاج ',
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                              shadows: [
                                                BoxShadow(
                                                    offset: Offset(4, 4),
                                                    blurRadius: 15,
                                                    color: Colors.grey),
                                                BoxShadow(
                                                    offset: Offset(-4, -4),
                                                    blurRadius: 15,
                                                    color: Colors.white60)
                                              ]),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(30)),
                              // ignore: prefer_const_literals_to_create_immutables
                              boxShadow: [
                                BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                BoxShadow(
                                    offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                              ]),
                          child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.95,
                                height: MediaQuery.of(context).size.height * 0.95,
                                child: ListView.builder(
                                    scrollDirection: Axis.vertical,
                                    itemCount: allNewsList.length,
                                    itemBuilder: (context, index) => newsCard(
                                          newsDetails: allNewsList[index].Details,
                                          newsTitle: allNewsList[index].Title,
                                          username: allNewsList[index].ActorName,
                                        )),
                              )),
                        ),
                      ]),
                    ),
                  ));
            },
            whenNotDone: const Center(
                child: CircularProgressIndicator(
              color: Colors.blue,
            )),
          ),
        ));
  }
}

class newsCard extends StatelessWidget {
  String? username;
  String? newsTitle;
  String? newsDetails;

  newsCard({this.username, this.newsTitle, this.newsDetails});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white /*Color(0xffEBEBEB)*/,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.white, width: 2),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        Text(
                          username!,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            color: Colors.blue,
                          ),
                        ),
                        Icon(
                          Icons.person,
                          size: 30,
                          color: Colors.blue,
                        ),
                      ],
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.edit,
                        size: 20,
                      ),
                      color: Colors.blue,
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.delete,
                        size: 20,
                      ),
                      color: Colors.red,
                      onPressed: () {},
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            SelectableText(
              newsTitle!,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black87,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            SelectableText(
              newsDetails!,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                color: Colors.black87,
              ),
            ),
          ],
        ),
      ),
    );
    ;
  }
}
