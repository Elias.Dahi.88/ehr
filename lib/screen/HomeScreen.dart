import 'package:ehr/screen/newsScreen.dart';
import 'package:ehr/screen/Get_Pationt.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:flutter/material.dart';
import 'diseaseScreen.dart';
import 'prescriptionScreen.dart';
import 'anlaysise_Screen.dart';

enum UserType { dr, pk, lp, ad, usernom }

class HOS_home extends StatefulWidget {
  const HOS_home({
    Key? key,
  }) : super(key: key);

  @override
  State<HOS_home> createState() => _HOS_homeState();
}

class _HOS_homeState extends State<HOS_home> {
  int curntTab = 0;
  final List<Widget> screen = [
    news(),
    Hos_prescription(),
    analyssise(),
    disease(),
    Get_Pationt(),
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentscreen = (SharedClass.isDoctorAccount == true) ? Get_Pationt() : news();
  final is_embty = GlobalKey<State<Get_Pationt>>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: PageStorage(
        child: currentscreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.screen_search_desktop_outlined),
        onPressed: () {
          if (SharedClass.isDoctorAccount == true) {
            setState(() {
              currentscreen = Get_Pationt();
              curntTab = 0;
            });
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 5,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentscreen = news();
                        curntTab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.chrome_reader_mode_outlined,
                          color: curntTab == 0 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          ' الأخبار',
                          style: TextStyle(color: curntTab == 0 ? Colors.blue : Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                      minWidth: 40,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.medical_services_outlined,
                            color: curntTab == 1 ? Colors.blue : Colors.grey,
                          ),
                          Text(
                            'أمراض',
                            style: TextStyle(
                                color: curntTab == 1 ? Colors.blue : Colors.grey, fontSize: 12),
                          )
                        ],
                      ),
                      onPressed: () {
                        setState(() {
                          currentscreen = disease();
                          curntTab = 1;
                        });
                      })
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentscreen = Hos_prescription();
                        curntTab = 3;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.local_pharmacy_outlined,
                          color: curntTab == 3 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          'وصفات',
                          style: TextStyle(color: curntTab == 3 ? Colors.blue : Colors.grey),
                        ),
                      ],
                    ),
                  ),
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentscreen = analyssise();
                        curntTab = 4;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(
                          Icons.analytics_outlined,
                          color: curntTab == 4 ? Colors.blue : Colors.grey,
                        ),
                        Text(
                          'تحاليل',
                          style: TextStyle(color: curntTab == 4 ? Colors.blue : Colors.grey),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
