import 'package:flutter/material.dart';

class detail_disease extends StatelessWidget {
  List<String>? diseasesList = [];
  String? doctorName = "";

  detail_disease({this.diseasesList, this.doctorName});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: (GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: GestureDetector(
                          child: Container(
                            height: 75,
                            width: MediaQuery.of(context).size.width * 0.9,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                // ignore: prefer_const_literals_to_create_immutables
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                  BoxShadow(
                                      offset: Offset(-4, -4),
                                      blurRadius: 15,
                                      color: Colors.white60),
                                ]),
                            child: Padding(
                              padding: const EdgeInsets.all(15),
                              child: Row(
                                children: [
                                  Container(
                                    height: 50,
                                    width: 250,
                                    child: Text(
                                      ' اسم الدكتور : ${doctorName} ',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          shadows: [
                                            BoxShadow(
                                                offset: Offset(4, 4),
                                                blurRadius: 15,
                                                color: Colors.grey),
                                            BoxShadow(
                                                offset: Offset(-4, -4),
                                                blurRadius: 15,
                                                color: Colors.white60)
                                          ]),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: MediaQuery.of(context).size.height * 0.85,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            // ignore: prefer_const_literals_to_create_immutables
                            boxShadow: [
                              BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                              BoxShadow(
                                  offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                            ]),
                        child: Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.95,
                              height: MediaQuery.of(context).size.height * 0.95,
                              child: ListView.builder(
                                  itemCount: diseasesList!.length,
                                  itemBuilder: (context, index) => DiseaseCard(
                                        text: diseasesList![index],
                                      )),
                            )),
                      ),
                    ]),
                  ),
                )))),
      ),
    );
  }
}

class DiseaseCard extends StatelessWidget {
  String? text = "";

  DiseaseCard({this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border: Border.all(color: Colors.white, width: 2),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 25,
            ),
            Divider(
              height: 25,
            ),
            SelectableText(
              text!,
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 18, color: Colors.black87, fontWeight: FontWeight.w700),
            ),
            Divider(
              height: 25,
            ),
          ],
        ),
      ),
    );
    ;
  }
}
