import 'package:ehr/model/Patient/Perscription/Drugs.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:ehr/widgets/ViewDruge.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:flutter/material.dart';
import 'package:ehr/model/PrescriptionMedicine.dart';
import '../widgets/AlternativeMedicine.dart';

class detail_prscription extends StatefulWidget {
  String? doctorName;
  int? perspictionID;

  detail_prscription({this.doctorName, this.perspictionID});

  @override
  State<detail_prscription> createState() => _detail_prscriptionState();
}

class _detail_prscriptionState extends State<detail_prscription> {
  List<Drugs> vertual_druge = [];

  List<Drugs> final_druge = [];

  int? max, min;

  String? selecteditem;

  List _VALUELIST = ['dr', 'ph', 'lp', 'hosp', 'ad', 'select one'];

  List<PrescriptionMedicine> prescriptionMedicineList = [];
  Map<String, dynamic> bodyRequest = {};
  var _future;

  @override
  void initState() {
    bodyRequest = {};

    bodyRequest['PrescriptionID'] = widget.perspictionID!;
    _future = server
        .getPatientRelatedAPIs(bodyRequest: bodyRequest, url: server.ViewPrescriptionMedicineURL)
        .then((value) {
      prescriptionMedicineList = (value.map((e) => PrescriptionMedicine.fromJSON(e))).toList();
    });
    // TODO: implement initState
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: EnhancedFutureBuilder(
        future: _future,
        rememberFutureResult: false,
        whenDone: (whenDone) {
          return SafeArea(
              child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(25, 25, 15, 15),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      // ignore: prefer_const_literals_to_create_immutables
                      boxShadow: [
                        BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                        BoxShadow(offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                      ]),
                  child: Center(
                      child: Padding(
                          padding: const EdgeInsets.fromLTRB(5, 20, 5, 0),
                          child: Column(children: [
                            Container(
                              height: 50,
                              width: 250,
                              child: Center(
                                child: Text("الدكتور : " + widget.doctorName!,
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black)),
                              ),
                            ),
                            Divider(
                              height: 10,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.5,
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                              child: ListView.builder(
                                  itemCount: prescriptionMedicineList.length,
                                  itemBuilder: (context, index) => Container(
                                        height: 130.0,
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                          // ignore: prefer_const_literals_to_create_immutables
                                        ),
                                        child: Column(
                                          children: [
                                            ViewDruge(
                                                dosage: prescriptionMedicineList[index].dosage,
                                                medicineNAme:
                                                    prescriptionMedicineList[index].MedicineName,
                                                period: prescriptionMedicineList[index].Period),
                                            AlternativeMedicine(
                                              dosage: prescriptionMedicineList[index].dosage,
                                              medicineNAme: prescriptionMedicineList[index].AltName,
                                              period: prescriptionMedicineList[index].Period,
                                            ),
                                            Divider(
                                              height: 10,
                                            )
                                          ],
                                        ),
                                      )),
                            ),
                            if (SharedClass.isPahrmacistAccount == true)
                              Container(
                                  height: 40,
                                  width: 150,
                                  decoration: BoxDecoration(
                                    color: Colors.blue,
                                  ),
                                  child: TextButton.icon(
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.save,
                                        color: Colors.white,
                                      ),
                                      label: Text(
                                        'save',
                                        style: TextStyle(color: Colors.white),
                                      ))),
                          ]))),
                ),
              ),
            ),
          ));
        },
        whenNotDone: const Center(
          child: CircularProgressIndicator(
            color: Colors.blue,
          ),
        ),
      ),
    );
  }
}
