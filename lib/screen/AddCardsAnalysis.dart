import 'package:ehr/model/Patient/analysis/analysis.dart';
import 'package:ehr/model/Patient/analysis/model_card_analysise.dart';
import 'package:ehr/widgets/analysistypes.dart';
import 'package:ehr/widgets/cardaddanalysis.dart';
import 'package:flutter/material.dart';

class AddCardsAnalysis extends StatefulWidget {
  @override
  State<AddCardsAnalysis> createState() => _AddCardsAnalysisState();
}

class _AddCardsAnalysisState extends State<AddCardsAnalysis> {
  List<Analysis> vertual_analysis = [];
  List<Analysis> final_analysis = [];

  List<Analysis> vertual_image = [];
  List<Analysis> final_image = [];
  int? max, min;
  String? selecteditem;
  List _VALUELIST = ['dr', 'ph', 'lp', 'hosp', 'ad', 'select one'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 25, 15, 25),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    // ignore: prefer_const_literals_to_create_immutables
                    boxShadow: [
                      BoxShadow(offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                      BoxShadow(offset: Offset(-4, -4), blurRadius: 15, color: Colors.white60),
                    ]),
                child: Center(
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 20, 5, 0),
                        child: Column(children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 1),
                            child: Container(
                              height: 40,
                              width: 250,
                              child: Text('إضافة تحاليل جديدة  ',
                                  textAlign: TextAlign.right,
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 1),
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 50.0),
                                  child: Container(
                                    height: 40,
                                    width: 250,
                                    child: Text('لسيد/ة : محمد أحمد',
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ),
                                ),
                                IconButton(
                                  onPressed: () {
                                    showModalBottomSheet(
                                      backgroundColor: Colors.transparent,
                                      barrierColor: Colors.white38,
                                      context: context,
                                      builder: (context) {
                                        return Scaffold(
                                          body: Center(
                                            child: SingleChildScrollView(
                                              child: Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.symmetric(horizontal: 30),
                                                  child: Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    children: [
                                                      TextField(
                                                        //controller: _postTextTD,
                                                        textAlign: TextAlign.start,
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                        ),
                                                        keyboardType: TextInputType.multiline,
                                                        maxLines: null,
                                                        decoration: InputDecoration(
                                                          hintText: 'النص',
                                                        ),
                                                      ),
                                                      SizedBox(height: 20),
                                                      ElevatedButton(
                                                        onPressed: () {
                                                          // _addPost();
                                                        },
                                                        child: Text(
                                                          'أضف وصف عام لتحاليل',
                                                          style: TextStyle(
                                                            fontSize: 25,
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  icon: Icon(Icons.edit),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            height: 10,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Column(
                            children: [
                              Row(
                                children: [
                                  Padding(padding: const EdgeInsets.only(right: 30)),
                                  Container(
                                      height: 40.0,
                                      width: 125.0,
                                      color: Colors.blue,
                                      child: TextButton.icon(
                                          onPressed: () {
                                            setState(() {
                                              vertual_analysis.add(null!);
                                            });
                                          },
                                          icon: Icon(
                                            Icons.analytics_outlined,
                                            color: Colors.white,
                                            size: 30,
                                          ),
                                          label: Text(' تحاليل ',
                                              style: TextStyle(color: Colors.white)))),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      height: 40.0,
                                      width: 150.0,
                                      color: Colors.blue,
                                      child: TextButton.icon(
                                          onPressed: () {
                                            setState(() {
                                              vertual_image.add(null!);
                                            });
                                          },
                                          icon: Icon(Icons.add_a_photo_rounded,
                                              color: Colors.white, size: 30),
                                          label: Text(' صورة أشعة',
                                              style: TextStyle(color: Colors.white)))),
                                ],
                              ),
                              Divider(
                                height: 20,
                              ),
                              Container(
                                height: 300,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                ),
                                child: ListView.builder(
                                    itemCount: vertual_analysis.length,
                                    itemBuilder: (context, index) => Container(
                                          height: 150.0,
                                          width: MediaQuery.of(context).size.width,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(Radius.circular(30)),
                                            // ignore: prefer_const_literals_to_create_immutables
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.only(right: 15, left: 15),
                                            child: Column(
                                              children: [
                                                cardaddanalysis(),
                                                analysisTypes(max: max, min: min),
                                              ],
                                            ),
                                          ),
                                        )),
                              ),
                              Divider(
                                height: 15,
                              ),
                              Container(
                                  height: 200,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                  ),
                                  child: ListView.builder(
                                    itemCount: vertual_image.length,
                                    itemBuilder: (context, index) => Container(
                                        height: 75.0,
                                        width: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(Radius.circular(30)),
                                          // ignore: prefer_const_literals_to_create_immutables
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(right: 15, left: 15),
                                          child: Column(
                                            children: [
                                              Container(
                                                  height: 50,
                                                  child: TextField(
                                                    decoration: InputDecoration(
                                                      labelText: 'أدخل اسم الصورة المطلوبة ',
                                                    ),
                                                  )),
                                            ],
                                          ),
                                        )),
                                  ))
                            ],
                          ),
                          Divider(
                            height: 15,
                          ),
                          Container(
                            height: 40.0,
                            width: 125.0,
                            color: Colors.blue,
                            child: TextButton.icon(
                                onPressed: () {
                                  setState(() {});
                                },
                                icon: Icon(
                                  Icons.save,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                label: Text(' Save ', style: TextStyle(color: Colors.white))),
                          ),
                          SizedBox(
                            height: 20,
                          )
                        ])))),
          ),
        ),
      ),
    ));
  }
}
