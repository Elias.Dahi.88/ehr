import 'package:ehr/model/ImageItem.dart';
import 'package:ehr/model/PatientRequestImage.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../model/PatientAnalyze.dart';

class anlayss_detail extends StatefulWidget {
  @override
  State<anlayss_detail> createState() => _anlayss_detailState();
}

class _anlayss_detailState extends State<anlayss_detail> {
  var _future;
  void getAllData() {
    bodyRequest['PatientID'] = int.parse(SharedClass.LoginPatientID);

    _future = server
        .getPatientRelatedAPIs(bodyRequest: bodyRequest, url: server.ViewPatientRequestImageURL)
        .then((value) {
      patientRequestImageList = (value.map((e) => PatientRequestImage.fromJSON(e))).toList();

      doctorName = patientRequestImageList[0].DoctorName;
      patientName = patientRequestImageList[0].PatientName;

      bodyRequest = {};
      bodyRequest['PatientID'] = int.parse(SharedClass.LoginPatientID);
      server
          .getPatientRelatedAPIs(bodyRequest: bodyRequest, url: server.ViewPatientAnalyzeURL)
          .then((value) {
        patientAnalyzeList = (value.map((e) => PatientAnalyze.fromJSON(e))).toList();
        patientRequestImageList.forEach((element) {
          bodyRequest = {};
          bodyRequest['RequestImagesID'] = element.RequestImagesID;
          server
              .getPatientRelatedAPIs(url: server.ViewImageItemURL, bodyRequest: bodyRequest)
              .then((value) {
            imageItemsList = (value.map((e) => ImageItem.fromJSON(e))).toList();
            imageItemsList.forEach((element) {
              imagesURLsList.add(server.imageRoot + '/' + element.ImageUrl!);
            });
            setState(() {});
          });
        });
      });
    });
  }

  Map<String, dynamic> bodyRequest = {};
  String? patientName = "";
  String? doctorName = "";
  List<PatientAnalyze> patientAnalyzeList = [];
  List<PatientRequestImage> patientRequestImageList = [];
  List<String> imagesURLsList = [];
  List<ImageItem> imageItemsList = [];

  @override
  void initState() {
    super.initState();
    bodyRequest = {};
    getAllData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: EnhancedFutureBuilder(
        future: _future,
        rememberFutureResult: false,
        whenDone: (whenDone) {
          return SafeArea(
            child: GestureDetector(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);

                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                },
                child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Padding(
                        padding: const EdgeInsets.fromLTRB(25, 25, 25, 25),
                        child: Container(
                            height: MediaQuery.of(context).size.height * 0.9,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                // ignore: prefer_const_literals_to_create_immutables
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                  BoxShadow(
                                      offset: Offset(-4, -4),
                                      blurRadius: 15,
                                      color: Colors.white60),
                                ]),
                            child: Center(
                                child: Padding(
                              padding: const EdgeInsets.fromLTRB(5, 20, 5, 0),
                              child: SingleChildScrollView(
                                child: Column(children: [
                                  Padding(
                                    padding: const EdgeInsets.only(right: 1),
                                    child: Container(
                                      height: 50,
                                      width: 250,
                                      child: Text('اسم الدكتور : ' + doctorName!,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 1),
                                    child: Container(
                                      height: 40,
                                      width: 250,
                                      child: Text('اسم المريض : ' + patientName!,
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                  Divider(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Column(
                                    children: [
                                      Container(
                                        height: MediaQuery.of(context).size.height * 0.05,
                                        width: MediaQuery.of(context).size.width * 0.85,
                                        decoration: BoxDecoration(color: Colors.grey[300],
                                            // ignore: prefer_const_literals_to_create_immutables
                                            boxShadow: [
                                              BoxShadow(
                                                  offset: Offset(4, 4),
                                                  blurRadius: 15,
                                                  color: Colors.grey),
                                              BoxShadow(
                                                  offset: Offset(-4, -4),
                                                  blurRadius: 15,
                                                  color: Colors.white60),
                                            ]),
                                        child: Center(
                                          child: Text('سجل التحاليل',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black)),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Divider(
                                        height: 12,
                                      ),
                                      Container(
                                        height: 150,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                        ),
                                        child: ListView.builder(
                                            itemCount: patientAnalyzeList.length,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                height: 35.0,
                                                width: MediaQuery.of(context).size.width,
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.all(Radius.circular(30)),
                                                  // ignore: prefer_const_literals_to_create_immutables
                                                ),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(right: 15, left: 15),
                                                  child: Column(
                                                    children: [
                                                      Text(patientAnalyzeList[index].Details!),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            }),
                                      ),
                                    ],
                                  ),
                                  Divider(
                                    height: 10,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Column(
                                    children: [
                                      Container(
                                        height: MediaQuery.of(context).size.height * 0.05,
                                        width: MediaQuery.of(context).size.width * 0.85,
                                        decoration: BoxDecoration(color: Colors.grey[300],
                                            // ignore: prefer_const_literals_to_create_immutables
                                            boxShadow: [
                                              BoxShadow(
                                                  offset: Offset(4, 4),
                                                  blurRadius: 15,
                                                  color: Colors.grey),
                                              BoxShadow(
                                                  offset: Offset(-4, -4),
                                                  blurRadius: 15,
                                                  color: Colors.white60),
                                            ]),
                                        child: Center(
                                          child: Text('الصور الشعاعية',
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black)),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Divider(
                                        height: 12,
                                      ),
                                      Container(
                                          height: 150,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          child: ListView.builder(
                                            itemCount: patientRequestImageList.length,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                  //height: 40.0,
                                                  width: 150,
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.all(Radius.circular(30)),
                                                    // ignore: prefer_const_literals_to_create_immutables
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(right: 15, left: 15),
                                                    child: Column(
                                                      children: [
                                                        Text(patientRequestImageList[index]
                                                            .Details!),
                                                        CachedNetworkImage(
                                                          // imageUrl: imagesURLsList[index],
                                                          imageUrl: imagesURLsList[index],
                                                          height: 200.0,
                                                        ),
                                                      ],
                                                    ),
                                                  ));
                                            },
                                          )),
                                    ],
                                  ),
                                ]),
                              ),
                            )))))),
          );
        },
        whenNotDone: const Center(
            child: CircularProgressIndicator(
          color: Colors.blue,
        )),
      ),
    );
  }
}
