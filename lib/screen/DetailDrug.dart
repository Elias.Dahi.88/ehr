import 'package:flutter/material.dart';

class DetailDruge extends StatelessWidget {
  const DetailDruge({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: GestureDetector(
              onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
        
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
              },
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 25, 15, 15),
            child: Container(
              height:MediaQuery.of(context).size.height*0.9,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  // ignore: prefer_const_literals_to_create_immutables
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                    BoxShadow(
                        offset: Offset(-4, -4),
                        blurRadius: 15,
                        color: Colors.white60),
                  ]),
                  child: Center(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(5, 20, 5, 0),
                   child: Column(
                    children: [
                       Container(
                         height: 40,
                         width: 250,
                         child: Center(
                           child: Text('سجل صرف الدواء  ',
                               textAlign: TextAlign.right,
                               style: TextStyle(
                                   fontSize: 16,
                                   fontWeight: FontWeight.bold,
                                   color: Colors.black)),
                         ),
                       ),
                      
                      Divider(
                        height: 10,
                      ),
                      SizedBox(
                        height: 5,
                      ),
        
          Container(
            height:40,
            child:Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Text('الكمية المطلوبة: 100',textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                ),
                SizedBox(width: 15,),
                Text('الكمية المصروفة: 50',textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                
              ],
            )
          ),
                      Divider(
                        height: 10,
                      ),
                      SizedBox(
                        height: 5,
                      ),
        
                     
                      Container(
                        height: MediaQuery.of(context).size.height*0.73,
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListView.builder(
                            itemCount:5,
                            itemBuilder: (context,index)=>
                            Container(height: 150.0,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.all(Radius.circular(30)),
                                        // ignore: prefer_const_literals_to_create_immutables
                                      ),
                              child:Column(
                                children: [
                                  Container(
                                    height: 40,
                                    child: Row(children: [
                                      Text('الصيدلاني : أحمد',textAlign: TextAlign.right,
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                              
               
                                            
                SizedBox(width: 15,),
                Text(' DateTime(2018,5,10),',textAlign: TextAlign.right,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                    ],),
                                  ),
                                  
                                         Container(
                                           height: 40,
                                           child: Row(
                                             children: [
                                               SizedBox(width: 5,),
                                                Text('الكمية المصروفة: 50',textAlign: TextAlign.right,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black)),
                                             ],
                                           )),
               
                                  Divider(height: 10,)
                                ],
                              ),
                            )),
                       
                      ),
                        
                      Container(
                        height: 40,
                        width: 120,
                        decoration: BoxDecoration(
                  color: Colors.blue,
                  ),
                        child: TextButton.icon(onPressed: (){}, icon: Icon(Icons.save,color: Colors.white,), label: Text('save',style: TextStyle(color: Colors.white),))),
                     
                    ])
                  )
            ),
          ),
              ),
            ),
        )));
  }
}