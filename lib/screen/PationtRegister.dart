import 'package:ehr/screen/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PationtRegister extends StatefulWidget {
  const PationtRegister({ Key? key }) : super(key: key);

  @override
  State<PationtRegister> createState() => _PationtRegisterState();
}

class _PationtRegisterState extends State<PationtRegister> {
    DateTime selectedDate = DateTime.now();
  bool datepicked = false;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime ? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        datepicked = true;
      });
  }
  
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 30),
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "إنشاء حساب جديد",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "يرجى إدراج المعلومات بعناية",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey[800],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Column(
                          children: [
                            makeInput(
                                label: "الاسم الأول", hinttext: 'مثال : محمد'),
                            makeInput(
                                label: "الاسم الأب", hinttext: 'مثال : احمد'),
                            makeInput(
                                label: "الكنية", hinttext: 'مثال : المصري'),
                           
                            SizedBox(
                              height: 30,
                            ),
                           
                            makeInput(
                                label: "الايميل الشخصي",
                                hinttext: 'مثال : mhd@gmail.com'),
                            makeInput(
                                label: "الرقم الوطني",
                                hinttext: '******10140',
                                inputtype: TextInputType.number),
                            makeInput(
                                label: "رقم الهاتف",
                                hinttext: '********09',
                                inputtype: TextInputType.phone),
                            // makeInput(label: "تاريخ الميلاد",hinttext: "00/00/0000"),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      'تاريخ الميلاد',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            datepicked
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                          '${DateFormat.yMd().format(selectedDate)}',
                                          style: TextStyle(
                                            fontSize: 20,
                                          )),
                                      InkWell(
                                        onTap: () {
                                          _selectDate(context);
                                        },
                                        child: Text('تعديل',
                                            style: TextStyle(
                                              color: Colors.teal,
                                              fontSize: 20,
                                            )),
                                      ),
                                    ],
                                  )
                                : FlatButton(
                                    height: 30,
                                    color: Colors.teal[200],
                                    child: Text('اضغط لاختيار تاريخ ميلادك',
                                        style: TextStyle(
                                            fontSize: 15, color: Colors.white)),
                                    onPressed: () => _selectDate(context),
                                  ),
                            makeInput(
                                label: "كلمة السر",
                                obsureText: true,
                                hinttext: '●●●●●●'),
                            makeInput(
                                label: "تأكيد كلمة السر",
                                obsureText: true,
                                hinttext: '●●●●●●')
                          ],
                        ),
                      ),
                    
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Container(
                          padding: EdgeInsets.only(top: 3, left: 3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: Container(
                              height: 50,
                              width: 300,
                              child:  ElevatedButton(
                                 onPressed: () {
                                            
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(
                                      builder: (context) => HOS_home()));
                                        },
                                   child: Text('تسجيل'),
                                ),
                              ),
                              ),
                        ),
                      
                     
                      SizedBox(
                        height: 30,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget makeInput({label, obsureText = false, hinttext, inputtype, onFocus}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextField(
        obscureText: obsureText,
        keyboardType: inputtype,
        onTap: onFocus,
        decoration: InputDecoration(
          hintText: hinttext,
          hintStyle: TextStyle(color: Colors.grey),
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
              borderRadius: BorderRadius.circular(30)),
          border:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
        ),
      ),
      SizedBox(
        height: 30,
      )
    ],
  );
}
