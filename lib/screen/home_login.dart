// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:ehr/model/server.dart';
import 'package:ehr/screen/newsScreen.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:ehr/widgets/orginal_buttom.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:ehr/screen/Signup.dart';
import 'HomeScreen.dart';

class homepage extends StatefulWidget {
  homepage({
    Key? key,
  }) : super(key: key);

  @override
  State<homepage> createState() => _homepageState();
}

class _homepageState extends State<homepage> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController nationalNumber = TextEditingController();
  TextEditingController password = TextEditingController();
  String accountType = "doctor";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.4,
                        decoration: const BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(50),
                              bottomRight: Radius.circular(50),
                            )),
                      ),
                      Column(
                        children: <Widget>[
                          SizedBox(height: 50),
                          const Text(
                            'Welcom In E.H.R',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 28,
                              fontWeight: FontWeight.w600,
                              letterSpacing: 1.2,
                            ),
                          ),
                          Image.asset(
                            'assets/images/splash.png',
                            height: 150,
                          ),
                          SizedBox(height: 100),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 40),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 12),
                                Text('رقمك الوطني'),
                                TextFormField(
                                  validator: (value) => value!.isEmpty ? 'أدخل بيانات صحيحة' : null,
                                  controller: nationalNumber,
                                  decoration: const InputDecoration(
                                    labelText: '0325xxxxxxx',
                                    hintText: '0325xxxxxxx',
                                  ),
                                ),
                                const SizedBox(height: 15),
                                const Text('كلمة المرور'),
                                TextFormField(
                                  controller: password,
                                  validator: (value) => value!.length < 6
                                      ? 'كلمة السر يجب ان تكون اكبر من 6 احرف'
                                      : null,
                                  decoration: const InputDecoration(
                                    labelText: '●●●●●●',
                                  ),
                                  obscureText: true,
                                ),
                                const SizedBox(height: 18),
                                const Text('نوع الحساب'),
                                DropdownButton(
                                    items: [
                                      DropdownMenuItem(
                                        child: Text("دكتور"),
                                        value: "doctor",
                                      ),
                                      DropdownMenuItem(
                                        child: Text("مريض"),
                                        value: "patient",
                                      ),
                                    ],
                                    value: accountType,
                                    onChanged: (String? value) {
                                      setState(() {
                                        accountType = value!;
                                      });
                                    }),
                              ],
                            ),
                          ),
                          const SizedBox(height: 25),
                          OrginalButtom(
                            text: 'login',
                            onpressed: () async {
                              if (_formKey.currentState!.validate()) {
                                if (accountType == "patient") {
                                  final result = await server.LoginPatient(
                                      National_Number: nationalNumber.text,
                                      Password: password.text);

                                  if ((result != -1)) {
                                    SharedClass.LoginPatientID = result.toString();
                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(builder: (context) => HOS_home()));
                                  } else {
                                    AwesomeDialog(
                                      context: context,
                                      dialogType: DialogType.ERROR,
                                      animType: AnimType.BOTTOMSLIDE,
                                      title: 'خطأ في الطلب',
                                      desc: 'المعلومات المدخلة غير صحيحة !',
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {},
                                    ).show();
                                  }
                                } else {
                                  final result = await server.LoginDoctor(
                                      National_Number: nationalNumber.text,
                                      Password: password.text);

                                  if ((result != -1)) {
                                    SharedClass.LoginDoctorID = result.toString();
                                    SharedClass.LoginPatientID = "0";
                                    SharedClass.isDoctorAccount = true;
                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(builder: (context) => HOS_home()));
                                  } else {
                                    AwesomeDialog(
                                      context: context,
                                      dialogType: DialogType.ERROR,
                                      animType: AnimType.BOTTOMSLIDE,
                                      title: 'خطأ في الطلب',
                                      desc: 'المعلومات المدخلة غير صحيحة !',
                                      btnCancelOnPress: () {},
                                      btnOkOnPress: () {},
                                    ).show();
                                  }
                                }
                              }
                            },
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            "هل أنت من الوحدات الطبية ؟",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("ليس لديك حساب ؟ "),
                              InkWell(
                                onTap: () {
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) => SignUpPage()));
                                },
                                child: Text(
                                  "أنشئ حساباً جديداً",
                                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 50,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) => news()));
                            },
                            child: Text(
                              "أخر الاخبار الطبية",
                              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
