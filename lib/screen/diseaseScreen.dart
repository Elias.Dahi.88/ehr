import 'package:ehr/model/DiagnosesDisease.dart';
import 'package:ehr/model/PatientDiagnoses.dart';
import 'package:ehr/model/server.dart';
import 'package:ehr/shared/SharedClass.dart';
import 'package:ehr/widgets/AddDisease.dart';
import 'package:ehr/widgets/disease_card.dart';
import 'package:ehr/widgets/pationtCard.dart';
import 'package:flutter/material.dart';
import 'package:enhanced_future_builder/enhanced_future_builder.dart';
import 'package:ehr/screen/detail_disease_DOc.dart';
import 'package:provider/provider.dart';

class disease extends StatefulWidget {
  final List<bool>? is_embty;
  const disease({Key? key, this.is_embty}) : super(key: key);
  @override
  State<disease> createState() => _diseaseState();
}

class _diseaseState extends State<disease> {
  var Pationtname = "احمد";
  var PageTitle = 'الامراض';
  var image = 'assets/images/4.png';
  var Number = '03251010548';
  var AddName = 'عرب للتامين الصحي';
  var DrName = 'عبد الرحمن ';
  var Description = 'ألم عام في المفاصل ';

  int? itemCount = 2;

  List<PatientDiagnosis> patientDiagnosesList = [];
  List<String> diagnosisDiseaseList = [];

  Map<String, dynamic> bodyRequest = {};

  var _future;

  @override
  void dispose() {
    patientDiagnosesList = [];
    diagnosisDiseaseList = [];
    bodyRequest = {};
    print("dispose finish");
    super.dispose();
  }

  @override
  void initState() {
    bodyRequest['PatientID'] = int.parse(SharedClass.LoginPatientID);
    _future = server
        .getPatientRelatedAPIs(bodyRequest: bodyRequest, url: server.ViewPatientDiagnosisURL)
        .then((value) {
      patientDiagnosesList = (value.map((e) => PatientDiagnosis.fromJSON(e))).toList();
      print("patientDiagnosesList\n${patientDiagnosesList}");
      patientDiagnosesList.forEach((element) {
        bodyRequest = {};
        bodyRequest['DiagnosisID'] = element.DiagnosisID;
        server
            .getPatientRelatedAPIs(url: server.ViewDiagnosisDiseasesURL, bodyRequest: bodyRequest)
            .then((value2) {
          var temp = (value2.map((e) => DiagnosesDisease.fromJSON(e))).toList();
          temp.forEach((element) {
            if (!(diagnosisDiseaseList.contains(element.DiseaseName))) {
              diagnosisDiseaseList.add(element.DiseaseName!);
            }
          });
        });
      });

      diagnosisDiseaseList = diagnosisDiseaseList.toSet().toList();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: EnhancedFutureBuilder(
        future: _future,
        rememberFutureResult: false,
        whenDone: (whenDone) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 25, left: 25),
                  child: Column(
                    children: [
                      PationtCard(
                        Pationtname: Pationtname,
                        PageTitle: PageTitle,
                        Image: image,
                        Number: Number,
                        AdName: AddName,
                        onpressed: () {
                          showModalBottomSheet(
                            backgroundColor: Colors.transparent,
                            barrierColor: Colors.white38,
                            context: context,
                            builder: (context) {
                              return AddDisease();
                            },
                          );
                        },
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      SizedBox(
                        child: Expanded(
                          child: Container(
                            height: (MediaQuery.of(context).size.height * 0.25) * itemCount!,
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30),
                                ),
                                // ignore: prefer_const_literals_to_create_immutables
                                boxShadow: [
                                  BoxShadow(
                                      offset: Offset(4, 4), blurRadius: 15, color: Colors.grey),
                                  BoxShadow(
                                      offset: Offset(-4, -4),
                                      blurRadius: 15,
                                      color: Colors.white60),
                                ]),
                            child: Container(
                              margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
                              decoration: const BoxDecoration(
                                color: Colors.white,
                              ),
                              child: ListView.builder(
                                itemCount: patientDiagnosesList.length,
                                itemBuilder: (context, index) => disease_Card(
                                  DrName: patientDiagnosesList[index].DoctorName,
                                  Generaldescription: patientDiagnosesList[index].Details,
                                  press: () {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => detail_disease(
                                              diseasesList: diagnosisDiseaseList,
                                              doctorName: patientDiagnosesList[index].DoctorName,
                                            )));
                                  },
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        },
        whenNotDone: const Center(
            child: CircularProgressIndicator(
          color: Colors.blue,
        )),
      ),
    );
  }
}
