import 'package:ehr/screen/HomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ehr/screen/home_login.dart';
import 'package:ehr/model/server.dart';

import 'newsScreen.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _Name = TextEditingController();
  TextEditingController _Address = TextEditingController();
  TextEditingController _National_Number = TextEditingController();
  TextEditingController _Email = TextEditingController();
  TextEditingController _License_Number = TextEditingController();
  TextEditingController _User_Name = TextEditingController();
  TextEditingController _Password = TextEditingController();
  TextEditingController _CoPassword = TextEditingController();
  String? accountType = "dds";

  DateTime different = DateTime.now();
  _clearTextInput() {
    _Name.text = '';
    _Address.text = '';
    // _Type.text = '';
    accountType = 'fds';
    _National_Number.text = '';
    _Email.text = '';
    _License_Number.text = '';
    _User_Name.text = '';
    _Password.text = '';
  }

  _Register() {
    if (_Name.text.isEmpty ||
            _Address.text.isEmpty ||
            accountType!.isEmpty ||
            _National_Number.text.isEmpty ||
            _Email.text.isEmpty ||
            _License_Number.text.isEmpty ||
            _User_Name.text.isEmpty ||
            _Password.text.isEmpty
//        selectedDate.year==different.year
        ) {
      print('Empty Fielsd');
      return;
    } else {
      print("before Register");
      server.Register(
              birth_Date: selectedDate,
              Email: _Email.text,
              Name: _Name.text,
              National_Number: _National_Number.text,
              Password: _Password.text)
          .then((result) {
        print("before success");
        if (result == "Added Successfully!!") {
          print("after success");
          _clearTextInput();

          showModalBottomSheet(
            backgroundColor: Colors.transparent,
            barrierColor: Colors.white38,
            context: context,
            builder: (context) {
              return Center(
                child: Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30, bottom: 25),
                  child: Container(
                    height: 550,
                    decoration: BoxDecoration(
                        color: const Color(0xFFFFFFF).withOpacity(1.0),
                        border: Border.all(color: Colors.grey),
                        borderRadius: const BorderRadius.all(Radius.circular(30))),
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'ستصلك  رسالة تأكيد على إيميلك خلال  24 ساعة ',
                            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          const Padding(padding: EdgeInsets.only(bottom: 15.0)),
                          ElevatedButton(
                              onPressed: () => Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(builder: (context) => HOS_home())),
                              child: Text('OK')),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        } else {
          print("Register Failure");
          print(result.toString());
        }
      });
    }
  }

  ////////

  DateTime selectedDate = DateTime.now();
  bool datepicked = false;
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900, 8),
        lastDate: DateTime(2101));

    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        datepicked = true;
      });
  }

  List _VALUELIST = [
    'dr',
    'ph',
    'lp',
    'hosp',
    'ad',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 30),
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            "إنشاء حساب جديد",
                            style: TextStyle(
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "يرجى إدراج المعلومات بعناية",
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.grey[800],
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Column(
                          children: [
                            makeInput(
                                label: "الاسم الكامل",
                                hinttext: ' مثال : محمد حسن',
                                controllers: _Name,
                                confirmPassowrd: false),
                            makeInput(
                                label: "اسم المستخدم",
                                hinttext: ' مثال : mhasn@gmail.com',
                                controllers: _User_Name,
                                confirmPassowrd: false),

                            makeInput(
                                label: "العنوان",
                                hinttext: ' مثال : دمشق المزة',
                                controllers: _Address,
                                confirmPassowrd: false),

                            Row(
                              children: [
                                Text('نوع الاختصاص   '),
                                Expanded(
                                  child: SizedBox(
                                    height: 40,
                                    child: Container(
                                      padding: EdgeInsets.only(left: 16, right: 16),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(30),
                                        shape: BoxShape.rectangle,
                                      ),
                                      child: DropdownButton(
                                        key: _formKey,
                                        hint: Text("choose from list"),
                                        dropdownColor: Color(0xfff2f9fe),
                                        elevation: 5,
                                        icon: Icon(
                                          Icons.arrow_drop_down_circle_outlined,
                                          color: Colors.grey[250],
                                        ),
                                        iconSize: 25,
                                        isExpanded: true,
                                        underline: SizedBox(
                                          height: 10,
                                        ),
                                        style: TextStyle(fontSize: 18, color: Colors.black),
                                        autofocus: true,
                                        // value: 0,
                                        onChanged: (val) {
                                          setState(() {
                                            accountType = val.toString();
                                          });
                                        },
                                        items: _VALUELIST.map((List) {
                                          return DropdownMenuItem(value: List, child: Text(List));
                                        }).toList(),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            makeInput(
                                label: "رقم الترخيص",
                                hinttext: 'مثال :ط / ب /125364',
                                controllers: _License_Number,
                                confirmPassowrd: false),
                            makeInput(
                                label: "شركة التأمين الصحي",
                                hinttext: 'مثال :عرب للتأمين الصحي',
                                confirmPassowrd: false),
                            makeInput(
                                label: "الايميل الشخصي",
                                hinttext: 'مثال : mhd@gmail.com',
                                controllers: _Email,
                                confirmPassowrd: false),
                            makeInput(
                                label: "الرقم الوطني",
                                hinttext: '******10140',
                                inputtype: TextInputType.number,
                                controllers: _National_Number,
                                confirmPassowrd: false),

                            // makeInput(label: "تاريخ الميلاد",hinttext: "00/00/0000"),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      'تاريخ الميلاد',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            datepicked
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Text('${DateFormat.yMd().format(selectedDate)}',
                                          style: TextStyle(
                                            fontSize: 20,
                                          )),
                                      InkWell(
                                        onTap: () {
                                          _selectDate(context);
                                        },
                                        child: Text('تعديل',
                                            style: TextStyle(
                                              color: Colors.teal,
                                              fontSize: 20,
                                            )),
                                      ),
                                    ],
                                  )
                                : FlatButton(
                                    height: 30,
                                    color: Colors.teal[200],
                                    child: Text('اضغط لاختيار تاريخ ميلادك',
                                        style: TextStyle(fontSize: 15, color: Colors.white)),
                                    onPressed: () => _selectDate(context),
                                  ),
                            makeInput(
                                label: "كلمة السر",
                                obsureText: true,
                                hinttext: '●●●●●●',
                                controllers: _Password,
                                confirmPassowrd: false),
                            makeInput(
                                label: "تأكيد كلمة السر",
                                obsureText: true,
                                hinttext: '●●●●●●',
                                controllers: _CoPassword,
                                confirmPassowrd: true)
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 40),
                        child: Container(
                          padding: EdgeInsets.only(top: 3, left: 3),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: Container(
                            height: 50,
                            width: 300,
                            child: ElevatedButton(
                              onPressed: () {
                                _Register();
                              },
                              child: Text('تسجيل'),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("لديك حساب ؟ "),
                          InkWell(
                            onTap: () {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute(builder: (context) => homepage()));
                            },
                            child: Text(
                              "سجل الدخول",
                              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget makeInput(
    {label, obsureText = false, hinttext, inputtype, onFocus, controllers, bool? confirmPassowrd}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        label,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400, color: Colors.black87),
      ),
      SizedBox(
        height: 5,
      ),
      TextFormField(
        /*  validator: confirmPassowrd!== true && (value) => value!.length < 6
                                      ? 'كلمة السر يجب ان تكون اكبر من 6 احرف'
                                      : null ,*/

        obscureText: obsureText,
        keyboardType: inputtype,
        onTap: onFocus,
        controller: controllers,
        decoration: InputDecoration(
          hintText: hinttext,
          hintStyle: TextStyle(color: Colors.grey),
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey), borderRadius: BorderRadius.circular(30)),
          border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
        ),
      ),
      SizedBox(
        height: 30,
      )
    ],
  );
}
